export { default as email } from './email';
export { default as number } from './number';
export { default as regExp } from './regExp';
export { default as required } from './required';
export { default as sameAs } from './sameAs';
export { default as stringLength } from './stringLength';