'use strict';

export default function number(object, config) {
	const errors = {};
	const value = parseFloat(object[config.field]);

	if (isNaN(value) || !isFinite(value)) {
		errors[config.field] = config.message || config.type;
	}

	if (typeof config.min !== 'undefined') {
		if (value < parseFloat(config.min)) {
			errors[config.field] = config.message || config.type;
		}
	}

	if (typeof config.max !== 'undefined') {
		if (value > parseFloat(config.max)) {
			errors[config.field] = config.message || config.type;
		}
	}

	return errors;
}