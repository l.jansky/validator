'use strict';

export default function stringLength(object, config) {
	const errors = {};
	const value = object[config.field];

	if (typeof config.min !== 'undefined') {
		if (value.length < parseInt(config.min)) {
			errors[config.field] = config.message || config.type;
		}
	}

	if (typeof config.max !== 'undefined') {
		if (value.length > parseInt(config.max)) {
			errors[config.field] = config.message || config.type;
		}
	}

	return errors;

}