'use strict';

export default function required (object, config) {
	const errors = {};
	const value = object[config.field];
	if (typeof value === 'undefined' || value === null || value === '') {
		errors[config.field] = config.message || config.type;
	}

	return errors;
}
