import * as rules from './rules';

export function ruleFactoryGet (name) {
	return rules[name] || null;
}