import {ruleFactoryGet} from './ruleFactory';

export function validateObject (object, rules, patch = false) {

	const errors = {};
	rules.forEach(rule => {
		if (!patch || (patch && typeof object[rule.field] !== 'undefined')) {
			const ruleFunction = ruleFactoryGet(rule.type);
			Object.assign(errors, ruleFunction(object, rule));
		}
	});
	
	return errors;
}